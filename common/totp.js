
import { TOTP,Base32 } from 'jsotp/lib/jsotp';

/**
 * @param {Object} key生成一次性密码
 */
export function totpgener(key){
	try{
		// let b32_secret = Base32.random_gen();
		// uni.showModal({
		// 	content:b32_secret
		// })
		let totp = TOTP(key);
		 // => 432143	
		return totp.now();
	}catch(e){
		//TODO handle the exception
		uni.showModal({
			content:e
		})
	}
}
/**
 * 生成随机密码测试
 */
export function genrandom(){
	let key=Base32.random_gen();
	let totp = TOTP(key);
	 // => 432143	
	return totp.now();
}